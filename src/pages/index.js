import Nav from "@/components/nav";
import Layout from "@/components/layout";
import logo from "public/svgs/logo.svg"
import RtzNav from "@/components/rtz-nav";
import RtzFooter from "@/components/footer";

const linksData = [
    {
      title: "GAMES",
      navto: "/about-us",
    },
    {
      title: "HUB",
      navto: "/about-us",
    },
    {
      title: "TECH",
      navto: "/about-us",
    },
    {
      title: "E-SPORT",
      navto: "/about-us",
    },
    {
      title: "ABOUT",
      navto: "/about-us",
    },
  ];

export default function HomePage() {
  
    return (
        <Layout>
            <RtzNav logo={logo} backgroundColor="black" navClass="navclass" links={linksData} linkColor="white" ctaText="DOWNLOAD HUB"/>
              <div className="homepage container mx-auto px-12">
                
              </div>
            <RtzFooter />
        </Layout>
      
    );
  }