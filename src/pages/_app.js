import '../assets/scss/main.scss'
import "../assets/scss/nav.scss"
import "../assets/scss/global.css"
import "../assets/scss/footer.scss"
import "../assets/scss/homepage.scss"

function MyApp({ Component, pageProps }) {
  return <Component {...pageProps} />
}

export default MyApp
