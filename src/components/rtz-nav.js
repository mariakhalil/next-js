import Link from 'next/link';
import Image from "next/image";
import ctaBorder from "public/svgs/rectangle.svg"


export default function RtzNav({logo, backgroundColor, navClass, links, linkColor, ctaText}) {
  
    return (
      <nav className={`navbar flex items-center ${navClass}`} style={{backgroundColor: backgroundColor}}>
          <div className='container mx-auto flex justify-between items-center px-12'>
            <div className=''>
                <a href="">
                  <Image
                        src={logo}
                        width={170}
                        height={170}
                        className='my-3'
                  />
                </a>
            </div>
            <div className='flex justify-between items-center'>
              {links.map((item,i) =>{
                return(
                  <Link className="navbar-links" style={{color: linkColor}} href={item.navto}>{item.title}</Link>
                );
              })}
              <a href="" className='navbar-cta-btn ml-5 relative'>
                <Image src={ctaBorder} className=""/>
                <div className='absolute cta-text'>
                    {ctaText}
                </div>
              </a>
            </div>
          </div>
        </nav>
    );
  }