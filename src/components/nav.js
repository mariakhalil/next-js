import Link from 'next/link';
import Image from "next/image";



export default function Nav({logo, backgroundColor, navClass, links, linkColor}) {
  
    return (
      <nav className={`navbar py-2 px-5 ${navClass}`} style={{backgroundColor: backgroundColor}}>
          <div className='container flex justify-between items-center'>
            <div className=''>
                <a href="">
                  <Image
                        src={logo}
                        width={150}
                        height={150}
                        className='my-3'
                  />
                </a>
            </div>
            <div className='flex justify-between items-center'>
              {links.map((item,i) =>{
                return(
                  <Link className="px-2 navbar-links" style={{color: linkColor}} href={item.navto}>{item.title}</Link>
                );
              })}
              <a href="" className='navbar-cta-btn ml-5'>
                CTA button
              </a>
            </div>
          </div>
        </nav>
    );
  }