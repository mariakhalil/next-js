import Link from 'next/link';
import Image from "next/image";

import logo from "public/svgs/logo.svg"
import discord from "public/svgs/discord.svg"
import facebook from "public/svgs/fb.svg"
import insta from "public/svgs/insta.svg"
import linkedin from "public/svgs/linkedin.svg"
import rtzSm from "public/svgs/logo-footer.svg"
import reddit from "public/svgs/reddit.svg"
import twitter from "public/svgs/twitter.svg"
import ctaBorder from "public/svgs/rectangle-white.svg"

export default function RtzFooter() {
  
    return (
        <div className='rtz-footer'>
            <div className='container mx-auto px-12 pt-14'>
                <div className='grid grid-cols-4'>
                    <div className='flex flex-col justify-between'>
                            <div>
                                <div className='footer-logo'>
                                    <a href="">
                                    <img src="/svgs/logo.svg"/>
                                    </a>
                                </div>
                                <div className='flex pt-3'>
                                    <a href="" className='mr-4'>
                                        <Image src={discord} width={25} height={23} className='my-3' />
                                    </a>
                                    <a href="" className='mr-4'>
                                        <Image src={twitter} width={25} height={23} className='my-3' />
                                    </a>
                                    <a href="" className='mr-4'>
                                        <Image src={rtzSm} width={25} height={23} className='my-3' />
                                    </a>
                                    <a href="" className='mr-4'>
                                        <Image src={insta} width={25} height={23} className='my-3' />
                                    </a>
                                    <a href="" className='mr-4'>
                                        <Image src={linkedin} width={25} height={23} className='my-3' />
                                    </a>
                                    <a href="" className='mr-4'>
                                        <Image src={reddit} width={25} height={23} className='my-3' />
                                    </a>
                                    <a href="" className=''>
                                        <Image src={facebook} width={25} height={23} className='my-3' />
                                    </a>
                                </div> 
                            </div>
                            <div className=''>
                                <div className='privacy-terms'>
                                    <a href="">Privacy</a> - <a href="">Terms</a> &nbsp;&nbsp;  © 2023
                                </div>
                            </div>
                    </div>
                    <div className='col-span-2 ml-20'>
                        <div className='grid grid-cols-2'>
                            <div className=''>
                                <div className='flex flex-col'>
                                    <div className='title'>ROLLING THUNDERS</div>
                                    <div className='flex flex-col subtitles'>
                                        <div className='pt-7'><a href="">GAMES</a></div>
                                        <div className='pt-7'><a href="">HUB</a></div>
                                        <div className='pt-7'><a href="">TECH</a></div>
                                        <div className='pt-7'><a href="">E-SPORT</a></div>
                                        <div className='pt-7'><a href="">ABOUT</a></div>
                                    </div>
                                </div>
                            </div>
                            <div className='ml-20'>
                                <div className='flex flex-col'>
                                    <div className='title'>GAME</div>
                                    <div className='flex flex-col subtitles'>
                                        <div className='pt-7'><a href="">WEB3WAR</a></div>
                                        <div className='pt-7'><a href="">GAME 2 HERE</a></div>
                                        <div className='pt-7'><a href="">GAME 3 HERE</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className='ml-20'>
                        <a href="" className='footer-cta-btn relative'>
                            <Image src={ctaBorder} className=""/>
                            <div className='absolute cta-text'>
                            ROLLING THUND3RS
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    );
  }